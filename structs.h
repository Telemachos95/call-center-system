#ifndef STRUCTS_H_INCLUDED
#define STRUCTS_H_INCLUDED

struct cdr {
char *cdr_uniq_id;
char originator_number[15];
char destination_number[15];
char date[9];                                       //The call detail record struct used for storing cdr's
char init_time[6];
int duration;
int type;
int tarrif;
int fault_condition;
};

#endif // STRUCTS_H_INCLUDED
