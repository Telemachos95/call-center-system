#ifndef CHAINED_HASHING_H
#define	CHAINED_HASHING_H

#include <time.h>
#include "structs.h"

struct hashNode {
    char number[15];
    struct list *list;              //A bucket node that stores a number (Originator,Destination),a linked list that include all cdr's for this number,and next points to the next bucket node on this particular bucket.
    struct hashNode *next;
};

struct hash {
    struct hashNode *head;          //Equivalent to a bucket,we create as many as we want our bucket size to be.We call head the first bucket node of each bucket.
};

void createNode(struct cdr *ptr,int table);
void insert(struct cdr *ptr, struct hash *hashTable,int table,int HashtableNumOfEntries);
void delete_cdr(struct cdr *ptr, struct hash *hashTable,int table,int HashtableNumOfEntries);
struct hashNode* get_caller_info(struct cdr *ptr,struct hash *hashTable,int table,int HashtableNumOfEntries);
void find_caller(struct cdr *ptr,struct hash *hashTable1,char time1[6], char date1[9], char time2[6], char date2[9],int condition,int table,int Hashtable1NumOfEntries);
void indist1(char number1[15],char number2[15],struct hash *hashTable1,struct hash *hashTable2,int Hashtable1NumOfEntries,int Hashtable2NumOfEntries);
void topdest(char caller[15],struct hash *hashTable,int HashtableNumOfEntries);
void top(int k);
int check_number(struct cdr *ptr, struct hash *hashTable,int table,int HashtableNumOfEntries);
void print(struct hash *hashTable,int table,int HashtableNumOfEntries);
int hash_function(const char* word, int size);
int time_to_seconds(char time[6]);
struct tm * date_converter(char date[9]);

#endif
