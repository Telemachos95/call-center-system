#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "structs.h"
#include "chained_hashing.h"

int main(int argc,char** argv) {

    FILE * fp;
    char * line = NULL;
    size_t len = 0;
    ssize_t read;
    char filename[20];
    int Hashtable1NumOfEntries;
    int Hashtable2NumOfEntries;

    if (argc != 7) {
        printf("Correct syntax is: %s -o Operations -h1 Hashtable1NumOfEntries -h2 Hashtable2NumOfEntries\n", argv[0]);
        return -1;
    }


    if(strcmp(argv[1],"-o")==0 && strcmp(argv[3],"-h1")==0 && strcmp(argv[5],"-h2")==0) {
        strcpy(filename,argv[2]);
        Hashtable1NumOfEntries = atoi(argv[4]);
        Hashtable2NumOfEntries = atoi(argv[6]);
    } else  if(strcmp(argv[1],"-o")==0 && strcmp(argv[3],"-h2")==0 && strcmp(argv[5],"-h1")==0) {
        strcpy(filename,argv[2]);
        Hashtable1NumOfEntries = atoi(argv[6]);
        Hashtable2NumOfEntries = atoi(argv[4]);
    } else  if(strcmp(argv[1],"-h1")==0 && strcmp(argv[3],"-h2")==0 && strcmp(argv[5],"-o")==0) {
        strcpy(filename,argv[6]);
        Hashtable1NumOfEntries = atoi(argv[2]);
        Hashtable2NumOfEntries = atoi(argv[4]);
    }else  if(strcmp(argv[1],"-h1")==0 && strcmp(argv[3],"-o")==0 && strcmp(argv[5],"-h2")==0) {
        strcpy(filename,argv[4]);
        Hashtable1NumOfEntries = atoi(argv[2]);
        Hashtable2NumOfEntries = atoi(argv[6]);
    }else  if(strcmp(argv[1],"-h2")==0 && strcmp(argv[3],"-o")==0 && strcmp(argv[5],"-h1")==0) {
        strcpy(filename,argv[4]);
        Hashtable1NumOfEntries = atoi(argv[6]);
        Hashtable2NumOfEntries = atoi(argv[2]);
    }else  if(strcmp(argv[1],"-h2")==0 && strcmp(argv[3],"-h1")==0 && strcmp(argv[5],"-o")==0) {
        strcpy(filename,argv[6]);
        Hashtable1NumOfEntries = atoi(argv[4]);
        Hashtable2NumOfEntries = atoi(argv[2]);
    }

    fp = fopen(filename, "r");
    if (fp == NULL) {
        printf("error while opening record file");
    }

    struct hash *hashTable1;        //HashTable initialization.
    struct hash *hashTable2;

    hashTable1 = calloc(Hashtable1NumOfEntries,sizeof(struct hash));
    hashTable2 = calloc(Hashtable2NumOfEntries,sizeof(struct hash));

    int i;

    while ((read = getline(&line, &len, fp)) != -1) {       //Read operations file line by line and perform the operations.
        i = 1;
        strtok(line," ");

        if(strcmp(line,"insert")==0) {
            struct cdr *record = malloc(sizeof(struct cdr));
            while(line!=NULL) {
                line = strtok(NULL, ";");
                if(i==1) {
                    record->cdr_uniq_id = malloc((strlen(line)+1)*sizeof(char));
                    strcpy(record->cdr_uniq_id,line);
                } else if(i==2) {
                    strcpy(record->originator_number,line);
                } else if(i==3) {
                    strcpy(record->destination_number,line);
                } else if(i==4) {
                    strcpy(record->date,line);
                } else if(i==5) {
                    strcpy(record->init_time,line);
                } else if(i==6) {
                    int duration = atoi(line);
                    record->duration = duration;
                } else if(i==7) {
                    int type = atoi(line);
                    record->type = type;
                } else if(i==8) {
                    int tarrif = atoi(line);
                    record->tarrif = tarrif;
                } else if(i==9) {
                    int fault_condition = atoi(line);
                    record->fault_condition = fault_condition;
                } else {
                    break;
                }
                i++;
            }
            insert(record,hashTable1,1,Hashtable1NumOfEntries);            //Insert Operation
            insert(record,hashTable2,2,Hashtable2NumOfEntries);
            free(record->cdr_uniq_id);
            free(record);
        } else if(strcmp(line,"delete")==0) {
            struct cdr *record = malloc(sizeof(struct cdr));
            while(line!=NULL) {
                line = strtok(NULL, " ");
                if(i==1) {
                    strcpy(record->originator_number,line);
                } else if(i==2) {
                    record->cdr_uniq_id = malloc((strlen(line)+1)*sizeof(char));
                    char *newline = strchr( line, '\n' );                           //strtok adds a \n at the end of the extracted line for some reason...
                    if ( newline )
                        *newline = 0;
                    strcpy(record->cdr_uniq_id,line);
                } else {
                    break;
                }
                i++;
            }
            delete_cdr(record,hashTable1,1,Hashtable1NumOfEntries);        //Delete operation
            free(record->cdr_uniq_id);
            free(record);
        } else if(strcmp(line,"find")==0) {
            struct cdr *record = malloc(sizeof(struct cdr));
            int time_flag = 0;
            int condition = 0;
            char time1[6];
            char time2[6];
            char date1[9];
            char date2[9];
            while(line!=NULL) {
                line = strtok(NULL, " ");
                if(i==1) {
                    char *newline = strchr( line, '\n' );           //strtok adds a \n at the end of the extracted line for some reason...
                    if ( newline ) {
                        *newline = 0;
                        strcpy(record->originator_number,line);
                        condition = 1;
                        break;
                    }
                    strcpy(record->originator_number,line);
                } else if(i==2) {
                    if(strstr(line, ":") != NULL) {
                        strcpy(time1,line);
                        time_flag = 1;
                    } else {
                        strcpy(date1,line);
                    }
                } else if(i==3) {
                    if(time_flag==1) {
                        if(strstr(line, ":") != NULL) {
                            char *newline = strchr( line, '\n' );       //strtok adds a \n at the end of the extracted line for some reason...
                            if ( newline ) {
                                *newline = 0;
                                strcpy(time2,line);
                                condition = 2;
                                break;
                            }
                            strcpy(time2,line);
                        } else {
                            strcpy(date1,line);
                        }
                    } else {
                        if(strstr(line, ":") != NULL) {
                            strcpy(time1,line);
                        } else {
                            strcpy(date2,line);
                        }
                    }

                } else if(i==4) {
                    if(strstr(line, ":") != NULL) {
                        strcpy(time2,line);
                    } else {
                        strcpy(date1,line);
                    }
                } else if(i==5) {
                    if(strstr(line, ":") != NULL) {
                        strcpy(time2,line);
                    } else {
                        char *newline = strchr( line, '\n' );       //strtok adds a \n at the end of the extracted line for some reason...
                        if ( newline ) {
                            *newline = 0;
                            strcpy(date2,line);
                            condition = 4;
                            break;
                        }
                        strcpy(date2,line);
                    }
                } else {
                    break;
                }
                i++;
            }
            find_caller(record,hashTable1,time1,date1,time2,date2,condition,1,Hashtable1NumOfEntries);     //Find operation
            free(record);
        } else if(strcmp(line,"lookup")==0) {
            struct cdr *record = malloc(sizeof(struct cdr));
            int time_flag = 0;
            int condition = 0;
            char time1[6];
            char time2[6];
            char date1[9];
            char date2[9];
            while(line!=NULL) {
                line = strtok(NULL, " ");
                if(i==1) {
                    char *newline = strchr( line, '\n' );       //strtok adds a \n at the end of the extracted line for some reason...
                    if ( newline ) {
                        *newline = 0;
                        strcpy(record->destination_number,line);
                        condition = 1;
                        break;
                    }
                    strcpy(record->destination_number,line);
                } else if(i==2) {
                    if(strstr(line, ":") != NULL) {
                        strcpy(time1,line);
                        time_flag = 1;
                    } else {
                        strcpy(date1,line);
                    }
                } else if(i==3) {
                    if(time_flag==1) {
                        if(strstr(line, ":") != NULL) {
                            char *newline = strchr( line, '\n' );       //strtok adds a \n at the end of the extracted line for some reason...
                            if ( newline ) {
                                *newline = 0;
                                strcpy(time2,line);
                                condition = 2;
                                break;
                            }
                            strcpy(time2,line);
                        } else {
                            strcpy(date1,line);
                        }
                    } else {
                        if(strstr(line, ":") != NULL) {
                            strcpy(time1,line);
                        } else {
                            strcpy(date2,line);
                        }
                    }

                } else if(i==4) {
                    if(strstr(line, ":") != NULL) {
                        strcpy(time2,line);
                    } else {
                        strcpy(date1,line);
                    }
                } else if(i==5) {
                    if(strstr(line, ":") != NULL) {
                        strcpy(time2,line);
                    } else {
                        char *newline = strchr( line, '\n' );       //strtok adds a \n at the end of the extracted line for some reason...
                        if ( newline ) {
                            *newline = 0;
                            strcpy(date2,line);
                            condition = 4;
                            break;
                        }
                        strcpy(date2,line);
                    }
                } else {
                    break;
                }
                i++;
            }
            find_caller(record,hashTable2,time1,date1,time2,date2,condition,2,Hashtable2NumOfEntries);     //Lookup operation
            free(record);
        } else if(strcmp(line,"indist")==0) {
            char number1[15];
            char number2[15];
            while(line!=NULL) {
                line = strtok(NULL, " ");
                if(i==1) {
                    strcpy(number1,line);
                } else if(i==2) {
                    char *newline = strchr( line, '\n' );       //strtok adds a \n at the end of the extracted line for some reason...
                    if ( newline )
                        *newline = 0;
                    strcpy(number2,line);
                } else {
                    break;
                }
                i++;
            }
            indist1(number1,number2,hashTable1,hashTable2,Hashtable1NumOfEntries,Hashtable2NumOfEntries);     //Indist operation
        }  else if(strcmp(line,"topdest")==0) {
            char caller[15];
            while(line!=NULL) {
                line = strtok(NULL, " ");
                if(i==1) {
                    char *newline = strchr( line, '\n' );           //strtok adds a \n at the end of the extracted line for some reason...
                    if ( newline )
                        *newline = 0;
                    strcpy(caller,line);
                } else {
                    break;
                }
                i++;
            }
            topdest(caller,hashTable1,Hashtable1NumOfEntries);         //Topdest operation
        }  else if(strcmp(line,"bye")==0) {
            printf("Werhauz will free allocated memory\n");
            int i;
            struct hashNode *head = hashTable1[i].head;
            struct hashNode *curr;
            for(i=0; i<Hashtable1NumOfEntries; i++) {
                while ((curr = head) != NULL) {
                    head = head->next;
                    free(curr->list);
                    free (curr);
                }
            }
            i=0;
            head = hashTable2[i].head;
            for(i=0; i<Hashtable2NumOfEntries; i++) {
                while ((curr = head) != NULL) {
                    head = head->next;
                    free(curr->list);
                    free (curr);
                }
            }

            free(hashTable1);
            free(hashTable2);

        }  else if(strcmp(line,"print")==0) {
            char table[11];
            while(line!=NULL) {
                line = strtok(NULL, " ");
                if(i==1) {
                    char *newline = strchr( line, '\n' );           //strtok adds a \n at the end of the extracted line for some reason...
                    if ( newline )
                        *newline = 0;
                    strcpy(table,line);
                } else {
                    break;
                }
                i++;
            }
            if(strcmp(table,"hashtable1") == 0) {
                print(hashTable1,1,Hashtable1NumOfEntries);                    //Print hashTable operation
            } else {
                print(hashTable2,2,Hashtable2NumOfEntries);
            }
        }
    }

    fclose(fp);

    return 0;
}



