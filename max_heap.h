#ifndef MAX_HEAP_H
#define	MAX_HEAP_H

#include "structs.h"

typedef struct node {
    struct cdr *ptr;
} node ;

typedef struct maxHeap {
    int size ;
    node *elem ;
} maxHeap ;

maxHeap initMaxHeap(int size);

#endif
