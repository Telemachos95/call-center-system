#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include "chained_hashing.h"
#include "linked_list.h"


struct hashNode * createHashNode(struct cdr *ptr,int table) {       //Creates a bucket node containing the telephone number (depending on the hashTable it is going to be inserted).
    struct hashNode *newnode;
    newnode = malloc(sizeof (struct hashNode));

    if(table==1) {
        strcpy(newnode->number,ptr->originator_number);
    } else {
        strcpy(newnode->number,ptr->destination_number);
    }

    struct list *linked_list;
    linked_list = malloc(sizeof(struct list));
    newnode->list = linked_list;
    newnode->next = NULL;

    insertToList(ptr,newnode,table);

    return newnode;
}

void insert(struct cdr *ptr, struct hash *hashTable,int table,int HashtableNumOfEntries) {         //Insert operation for the HashTable
    int result = check_number(ptr,hashTable,table,HashtableNumOfEntries);
    if(result==0) {
        int number;
        if(table==1) {
            number = hash_function(ptr->originator_number,HashtableNumOfEntries);
        } else {
            number = hash_function(ptr->destination_number,HashtableNumOfEntries);
        }

        struct hashNode *newnode;
        newnode = createHashNode(ptr,table);

        if (!hashTable[number].head) {
            hashTable[number].head = newnode;
            return;
        }
        /* adding new node to the list */
        newnode->next = (hashTable[number].head);
        /*
         * update the head of the list and no of
         * nodes in the current bucket
         */
        hashTable[number].head = newnode;
    } else {
        struct hashNode *node = get_caller_info(ptr,hashTable,table,HashtableNumOfEntries);
        insertToList(ptr,node,table);
    }
}

void delete_cdr(struct cdr *ptr, struct hash *hashTable,int table,int HashtableNumOfEntries) {        //Delete operation of the HashTable depending on the cdr id.
    int number;
    if(table==1) {
        number = hash_function(ptr->originator_number,HashtableNumOfEntries);
    } else {
        number = hash_function(ptr->destination_number,HashtableNumOfEntries);
    }
    int flag = 0;
    struct hashNode *myNode;
    /* get the list head from current bucket */
    myNode = hashTable[number].head;

    printf("DELETE CALLER QUERY: \n");

    if (!myNode) {
        printf("No element with this phone number in the Hash Table\n");
        return;
    }
    while (myNode != NULL) {
        /* delete the node with given key */
        if (strcmp(myNode->number,ptr->originator_number)==0) {
            flag = 1;
            delete_listNode(ptr,myNode);
        }
        myNode = myNode->next;
    }
    if (flag==0) {
        printf("No element with this phone number in the Hash Table\n");
    }
}

struct hashNode* get_caller_info(struct cdr *ptr,struct hash *hashTable,int table,int HashtableNumOfEntries) {        //Used to make sure each number is added to the Hashtable only once.
    int number;
    if(table==1) {
        number = hash_function(ptr->originator_number,HashtableNumOfEntries);
    } else {
        number = hash_function(ptr->destination_number,HashtableNumOfEntries);
    }

    struct hashNode *myNode;
    myNode = hashTable[number].head;
    if (!myNode) {
        return NULL;
    }
    if(table==1) {
        while (myNode != NULL) {
            if (strcmp(myNode->number,ptr->originator_number)==0) {
                break;
            }
            myNode = myNode->next;
        }
    } else {
        while (myNode != NULL) {
            if (strcmp(myNode->number,ptr->destination_number)==0) {
                break;
            }
            myNode = myNode->next;
        }
    }
    return myNode;
}

void find_caller(struct cdr *ptr,struct hash *hashTable,char time1[6], char date1[9], char time2[6], char date2[9],int condition,int table,int HashtableNumOfEntries) {   //The find caller operation depends on the provided time and date parameters.

    int number;
    int flag = 0;
    int cond1_flag = 0;
    int cond2_flag = 0;
    int cond3_flag = 0;
    int cond4_flag = 0;

    if(table==1) {
        number = hash_function(ptr->originator_number,HashtableNumOfEntries);
    } else {
        number = hash_function(ptr->destination_number,HashtableNumOfEntries);
    }

    int time1_secs;
    int time2_secs;

    struct hashNode *myNode;
    myNode = hashTable[number].head;
    if (!myNode) {
        printf("No element with this phone number in the Hash Table\n\n");
        return;
    }

    if(table==1) {
        while (myNode != NULL) {
            if (strcmp(myNode->number,ptr->originator_number)==0) {
                flag = 1;
                break;
            }
            myNode = myNode->next;
        }
    } else {
        while (myNode != NULL) {
            if (strcmp(myNode->number,ptr->destination_number)==0) {
                flag = 1;
                break;
            }
            myNode = myNode->next;
        }
    }

    if(flag == 0) {
        printf("No caller found with this number\n\n");
        return;
    }

    struct listNode *myNode1;
    myNode1 = myNode->list->head;

    if(condition == 1) {

        if(myNode->list->records>0) {
            if(table==1) {
                printf("FIND CALLER QUERY: \n");
            } else {
                printf("LOOKUP CALLEE QUERY: \n");
            }
            while (myNode1 != NULL) {
                if(table == 1) {
                    printf("Telephone Number(Originator)\tCDR ID\t\tTelephone Number(Destination)\tDate\t\tTime\tDuration\tType\tTarrif\tFault Condition\n");
                } else {
                    printf("Telephone Number(Destination)\tCDR ID\t\tTelephone Number(Originator)\tDate\t\tTime\tDuration\tType\tTarrif\tFault Condition\n");
                }
                printf("%s\t\t\t%s\t%s\t\t\t%s\t%s\t%d\t\t%d\t%d\t%d\n", myNode->number,myNode1->cdr_uniq_id,myNode1->number,myNode1->date,myNode1->init_time,myNode1->duration,myNode1->type,myNode1->tarrif,myNode1->fault_condition);
                cond1_flag = 1;
                myNode1 = myNode1->next;
            }
            printf("\n");
        }
        if(cond1_flag == 0) {
            printf("No cdr data for this query\n\n");
        }
    }


    if(condition == 2) {

        time1_secs = time_to_seconds(time1);
        time2_secs = time_to_seconds(time2);

        if(myNode->list->records>0) {
            if(table==1) {
                printf("FIND CALLER QUERY: \n");
            } else {
                printf("LOOKUP CALLEE QUERY: \n");
            }
            while (myNode1 != NULL) {
                int tmp_seconds = time_to_seconds(myNode1->init_time);
                if (tmp_seconds >= time1_secs && tmp_seconds <= time2_secs) {
                    if(table==1) {
                        printf("Telephone Number(Originator)\tCDR ID\t\tTelephone Number(Destination)\tDate\t\tTime\tDuration\tType\tTarrif\tFault Condition\n");
                    } else {
                        printf("Telephone Number(Destination)\tCDR ID\t\tTelephone Number(Originator)\tDate\t\tTime\tDuration\tType\tTarrif\tFault Condition\n");
                    }
                    printf("%s\t\t\t%s\t%s\t\t\t%s\t%s\t%d\t\t%d\t%d\t%d\n", myNode->number,myNode1->cdr_uniq_id,myNode1->number,myNode1->date,myNode1->init_time,myNode1->duration,myNode1->type,myNode1->tarrif,myNode1->fault_condition);
                    cond2_flag = 1;
                }
                myNode1 = myNode1->next;
            }
            printf("\n");
        }
        if(cond2_flag == 0) {
            printf("No cdr data for this query\n\n");
        }
    }


    if(condition == 3) {

        struct tm *tmp = date_converter(date1);
        time_t conv_date1 = mktime(tmp);
        struct tm *tmp1 = date_converter(date2);
        time_t conv_date2 = mktime(tmp1);

        if(myNode->list->records>0) {
            if(table==1) {
                printf("FIND CALLER QUERY: \n");
            } else {
                printf("LOOKUP CALLEE QUERY: \n");
            }
            while (myNode1 != NULL) {

                struct tm *tmp2 = date_converter(myNode1->date);
                time_t conv_date3 = mktime(tmp2);
                double seconds1 = difftime(conv_date3, conv_date1);
                double seconds2 = difftime(conv_date2, conv_date3);
                if (seconds1 > 0 && seconds2 > 0) {
                    if(table==1) {
                        printf("Telephone Number(Originator)\tCDR ID\t\tTelephone Number(Destination)\tDate\t\tTime\tDuration\tType\tTarrif\tFault Condition\n");
                    } else {
                        printf("Telephone Number(Destination)\tCDR ID\t\tTelephone Number(Originator)\tDate\t\tTime\tDuration\tType\tTarrif\tFault Condition\n");
                    }
                    printf("%s\t\t\t%s\t%s\t\t\t%s\t%s\t%d\t\t%d\t%d\t%d\n", myNode->number,myNode1->cdr_uniq_id,myNode1->number,myNode1->date,myNode1->init_time,myNode1->duration,myNode1->type,myNode1->tarrif,myNode1->fault_condition);
                    cond3_flag = 1;
                }
                myNode1 = myNode1->next;
            }
            printf("\n");
        }
        if(cond3_flag == 0) {
            printf("No cdr data for this query\n\n");
        }

        free(tmp);
        free(tmp1);
    }

    if(condition == 4) {

        time1_secs = time_to_seconds(time1);
        time2_secs = time_to_seconds(time2);

        struct tm *tmp = date_converter(date1);
        time_t conv_date1 = mktime(tmp);
        struct tm *tmp1 = date_converter(date2);
        time_t conv_date2 = mktime(tmp1);

        if(myNode->list->records>0) {
            if(table==1) {
                printf("FIND CALLER QUERY: \n");
            } else {
                printf("LOOKUP CALLEE QUERY: \n");
            }
            while (myNode1 != NULL) {

                int tmp_seconds = time_to_seconds(myNode1->init_time);
                struct tm *tmp2 = date_converter(myNode1->date);
                time_t conv_date3 = mktime(tmp2);
                double seconds1 = difftime(conv_date3, conv_date1);
                double seconds2 = difftime(conv_date2, conv_date3);

                if (tmp_seconds >= time1_secs && tmp_seconds <= time2_secs && seconds1 > 0 && seconds2 > 0) {
                    if(table==1) {
                        printf("Telephone Number(Originator)\tCDR ID\t\tTelephone Number(Destination)\tDate\t\tTime\tDuration\tType\tTarrif\tFault Condition\n");
                    } else {
                        printf("Telephone Number(Destination)\tCDR ID\t\tTelephone Number(Originator)\tDate\t\tTime\tDuration\tType\tTarrif\tFault Condition\n");
                    }
                    printf("%s\t\t\t%s\t%s\t\t\t%s\t%s\t%d\t\t%d\t%d\t%d\n", myNode->number,myNode1->cdr_uniq_id,myNode1->number,myNode1->date,myNode1->init_time,myNode1->duration,myNode1->type,myNode1->tarrif,myNode1->fault_condition);
                    cond4_flag = 1;
                }
                myNode1 = myNode1->next;
            }
            printf("\n");
        }
        if(cond4_flag == 0) {
            printf("No cdr data for this query\n\n");
        }
    }

}

int check_number(struct cdr *ptr, struct hash *hashTable,int table,int HashtableNumOfEntries) {       //This function makes sure each number is added only once to the hashtable.
    int number;
    if(table==1) {
        number = hash_function(ptr->originator_number,HashtableNumOfEntries);
    } else {
        number = hash_function(ptr->destination_number,HashtableNumOfEntries);
    }

    int flag = 0;
    struct hashNode *myNode;
    myNode = hashTable[number].head;
    if (!myNode) {
        return 0;
    }
    if(table==1) {
        while (myNode != NULL) {
            if (strcmp(myNode->number,ptr->originator_number)==0) {
                flag = 1;
                break;
            }
            myNode = myNode->next;
        }
    } else {
        while (myNode != NULL) {
            if (strcmp(myNode->number,ptr->destination_number)==0) {
                flag = 1;
                break;
            }
            myNode = myNode->next;
        }
    }
    return flag;
}

void  indist1(char number1[15],char number2[15],struct hash *hashTable1,struct hash *hashTable2,int Hashtable1NumOfEntries,int Hashtable2NumOfEntries) { //The function serves the indist queries.
    int number1_table1,number1_table2;
    number1_table1 = hash_function(number1,Hashtable1NumOfEntries);
    number1_table2 = hash_function(number1,Hashtable2NumOfEntries);

    int number2_table1,number2_table2;
    number2_table1 = hash_function(number2,Hashtable1NumOfEntries);
    number2_table2 = hash_function(number2,Hashtable2NumOfEntries);

    struct hashNode *myNode_n1_t1;
    myNode_n1_t1 = hashTable1[number1_table1].head;

    while (myNode_n1_t1 != NULL) {
        if (strcmp(myNode_n1_t1->number,number1)==0) {
            break;
        }
        myNode_n1_t1 = myNode_n1_t1->next;
    }

    struct hashNode *myNode_n1_t2;
    myNode_n1_t2 = hashTable2[number1_table2].head;

    while (myNode_n1_t2 != NULL) {
        if (strcmp(myNode_n1_t2->number,number1)==0) {
            break;
        }
        myNode_n1_t2 = myNode_n1_t2->next;
    }

    struct hashNode *myNode_n2_t1;
    myNode_n2_t1 = hashTable1[number2_table1].head;

    while (myNode_n2_t1 != NULL) {
        if (strcmp(myNode_n2_t1->number,number2)==0) {
            break;
        }
        myNode_n2_t1 = myNode_n2_t1->next;
    }

    struct hashNode *myNode_n2_t2;
    myNode_n2_t2 = hashTable2[number2_table2].head;

    while (myNode_n2_t2 != NULL) {
        if (strcmp(myNode_n2_t2->number,number2)==0) {
            break;
        }
        myNode_n2_t2 = myNode_n2_t2->next;
    }


    struct list *linked_list_n1;
    linked_list_n1 = malloc(sizeof(struct list));
    linked_list_n1->records = 0;

    if(myNode_n1_t1 != NULL) {

        struct listNode *node_n1_t1 = myNode_n1_t1->list->head;

        while (node_n1_t1 != NULL) {
            if(linked_list_n1->records==0) {
                insertToListIndist(node_n1_t1->number,linked_list_n1);
            } else if(contains_number(node_n1_t1->number,linked_list_n1)==0) {
                insertToListIndist(node_n1_t1->number,linked_list_n1);
            }
            node_n1_t1 = node_n1_t1->next;
        }
    }

    if(myNode_n1_t2 != NULL) {

        struct listNode *node_n1_t2 = myNode_n1_t2->list->head;

        while (node_n1_t2 != NULL) {
            if(linked_list_n1->records==0) {
                insertToListIndist(node_n1_t2->number,linked_list_n1);
            } else if(contains_number(node_n1_t2->number,linked_list_n1)==0) {
                insertToListIndist(node_n1_t2->number,linked_list_n1);
            }
            node_n1_t2 = node_n1_t2->next;
        }
    }

    struct list *linked_list_n2;
    linked_list_n2 = malloc(sizeof(struct list));
    linked_list_n2->records = 0;

    if(myNode_n2_t1 != NULL) {
        struct listNode *node_n2_t1 = myNode_n2_t1->list->head;

        while (node_n2_t1 != NULL) {
            if(linked_list_n2->records==0) {
                insertToListIndist(node_n2_t1->number,linked_list_n2);
            } else if(contains_number(node_n2_t1->number,linked_list_n2)==0) {
                insertToListIndist(node_n2_t1->number,linked_list_n2);
            }
            node_n2_t1 = node_n2_t1->next;
        }
    }

    if(myNode_n2_t2 != NULL) {

        struct listNode *node_n2_t2 = myNode_n2_t2->list->head;

        while (node_n2_t2 != NULL) {
            if(linked_list_n2->records==0) {
                insertToListIndist(node_n2_t2->number,linked_list_n2);
            } else if(contains_number(node_n2_t2->number,linked_list_n2)==0) {
                insertToListIndist(node_n2_t2->number,linked_list_n2);
            }
            node_n2_t2 = node_n2_t2->next;
        }
    }

    int flag = 0;
    struct listNode *node = linked_list_n1->head;
    struct listNode *node1 = linked_list_n2->head;
    struct list *linked_list_result = malloc(sizeof(struct list));
    linked_list_result->records = 0;

    while (node != NULL) {
        while (node1 != NULL) {
            if(strcmp(node->number,node1->number) == 0) {
                insertToListIndist(node->number,linked_list_result);
            }
            node1 = node1->next;
        }
        if(node1==NULL) {
            node1 = linked_list_n2->head;
        }
        node = node->next;
    }

    if(linked_list_result->records > 0 ) {
        node = linked_list_result->head;
        while (node != NULL) {
            if(linked_list_result->records == 1) {
                printf("INDIST QUERY: \n");
                printf("%s\n\n",node->number);
                flag = 1;
            } else {
                node1 = node->next;
                struct cdr *temp = malloc(sizeof(struct cdr));
                strcpy(temp->originator_number,node->number);
                struct hashNode *temp1 = get_caller_info(temp,hashTable1,1,Hashtable1NumOfEntries);
                int result = contains_number(node1->number,temp1->list);

                strcpy(temp->destination_number,node1->number);
                temp1 = get_caller_info(temp,hashTable2,2,Hashtable2NumOfEntries);
                result = contains_number(node->number,temp1->list);

                if(result == 0) {
                    printf("INDIST QUERY: \n");
                    printf("%s\n\n",node->number);
                    flag = 1;
                }

                free(temp);
            }
            node = node->next;
        }
    }


    if(flag == 0) {
        printf("No results found for this indist query\n");
    }

    struct listNode *curr;
    struct listNode *head = linked_list_n1->head;
    while ((curr = head) != NULL) {
        head = head->next;
        free (curr);
    }

    head = linked_list_n2->head;
    while ((curr = head) != NULL) {
        head = head->next;
        free (curr);
    }

    head = linked_list_result->head;
    while ((curr = head) != NULL) {
        head = head->next;
        free (curr);
    }

    free(linked_list_n1);
    free(linked_list_n2);
    free(linked_list_result);

}

void  topdest(char caller[15],struct hash *hashTable,int HashtableNumOfEntries) {     //This function serves the top destination queries.
    int number;
    number = hash_function(caller,HashtableNumOfEntries);

    struct hashNode *myNode;
    myNode = hashTable[number].head;

    if (!myNode) {
        printf("No results for this topdest query\n");
        return;
    }

    while (myNode != NULL) {
        if (strcmp(myNode->number,caller)==0) {
            break;
        }
        myNode = myNode->next;
    }

    struct list *linked_list = malloc(sizeof(struct list));
    linked_list->records = 0;

    struct listNode *node = myNode->list->head;
    struct listNode *node1;

    while(node != NULL) {
        char subbuff[15];
        memcpy( subbuff, &node->number[0], 3 );
        subbuff[3] = '\0';
        insertToListIndist(subbuff,linked_list);
        node = node->next;
    }

    node = linked_list->head;
    node1 = linked_list->head;

    int counter = 0;
    int result_counter = 0;
    char result[15];

    while (node != NULL) {
        while (node1 != NULL) {
            if(strcmp(node->number,node1->number) == 0) {
                counter++;
            }
            node1 = node1->next;
        }
        if(counter >= result_counter) {
            result_counter = counter;
            strcpy(result,node->number);
        }
        if(node1 == NULL) {
            node1 = linked_list->head;
        }
        counter = 0;
        node = node->next;
    }

    printf("TOPDEST QUERY: \n");
    printf("Caller: %s\n",caller);
    printf("Top country code: %s Times called: %d\n\n",result,result_counter);

    struct listNode *curr;
    struct listNode *head = linked_list->head;
    while ((curr = head) != NULL) {
        head = head->next;
        free (curr);
    }

    free(linked_list);

}

void print(struct hash *hashTable,int table,int HashtableNumOfEntries) {          //This function serves the print(hashtable) queries.
    struct hashNode *myNode;
    int i;
    for (i = 0; i < HashtableNumOfEntries; i++) {

        myNode = hashTable[i].head;
        if (!myNode)
            continue;
        printf("\nData at index %d in Hash Table %d:\n", i,table);
        if(table==1) {
            printf("Telephone Number(Originator)\n");
        } else {
            printf("Telephone Number(Destination)\n");
        }
        printf("--------------------------------\n");
        while (myNode != NULL) {
            printf("%s\n\n", myNode->number);
            print_listNode(myNode,table);
            myNode = myNode->next;
        }
    }
}

int hash_function(const char* word, int size) {     //The hash function used for the hashtable.In our application we use the telephone number (originator/destination) in order to hash our data.
    unsigned int hash = 0;
    int i;
    for (i = 0; word[i] != '\0'; i++) {
        hash = 31 * hash + word[i];
    }
    return hash % size;
}

int time_to_seconds(char time[6]) {         //Turns time into seconds used by the find operation.
    char subbuff[3];
    memcpy( subbuff, &time[0], 2 );
    subbuff[2] = '\0';

    char subbuff1[3];
    memcpy( subbuff1, &time[3], 2 );
    subbuff1[2] = '\0';

    int time1_secs = atoi(subbuff);
    time1_secs = time1_secs * 3600;
    int time2_secs = atoi(subbuff1);
    time2_secs = time2_secs * 60;

    int result = time1_secs + time2_secs;
    return result;
}

struct tm * date_converter(char date[9]) {      //Turns a date string into a struct tm (time.h).Used by the find operation.
    char day[3];
    memcpy( day, &date[0], 2 );
    day[2] = '\0';

    char month[3];
    memcpy( month, &date[2], 2 );
    month[2] = '\0';

    char year[5];
    memcpy( year, &date[4], 4 );
    year[4] = '\0';

    struct tm *info = malloc(sizeof(struct tm));

    info->tm_mday = atoi(day);
    info->tm_mon = atoi(month) -1;
    info->tm_year = atoi(year) - 1900;
    info->tm_hour = 0;
    info->tm_min = 0;
    info->tm_sec = 1;
    info->tm_isdst = 0;

    return info;
}

