#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "structs.h"
#include "linked_list.h"

struct listNode * createLinkNode(struct cdr *ptr,int table) {  //Creates a linked list node with the rest cdr info (depending on the hashTable it is going to be inserted).
    struct listNode *newnode;
    newnode = malloc(sizeof (struct listNode));
    newnode->cdr_uniq_id = malloc((strlen(ptr->cdr_uniq_id)+1)*sizeof(char));
    strcpy(newnode->cdr_uniq_id,ptr->cdr_uniq_id);
    if(table==1) {
        strcpy(newnode->number,ptr->destination_number);
    } else {
        strcpy(newnode->number,ptr->originator_number);
    }
    strcpy(newnode->date,ptr->date);
    strcpy(newnode->init_time,ptr->init_time);
    newnode->duration = ptr->duration;
    newnode->type = ptr->type;
    newnode->tarrif = ptr->tarrif;
    newnode->fault_condition = ptr->fault_condition;
    newnode->next = NULL;

    return newnode;
}

void insertToList(struct cdr *ptr, struct hashNode *node,int table) {  //Insert operation for the linked list.
    struct listNode *newnode = createLinkNode(ptr,table);
    if (!node->list->head) {
        node->list->head = newnode;
        node->list->records++;
        return;
    }
    newnode->next = node->list->head;
    node->list->head = newnode;
    node->list->records++;
    return;
}

void  delete_listNode(struct cdr *ptr,struct hashNode *node) {      //Delete operation for the linked list depending on the cdr id.
    int flag = 0;
    struct listNode *temp, *myNode;
    myNode = node->list->head;
    if (!myNode) {
        printf("No cdr records with this number\n");
        return;
    }
    temp = myNode;
    while (myNode != NULL) {
        if (strcmp(ptr->cdr_uniq_id,myNode->cdr_uniq_id)==0) {
            flag = 1;
            if (myNode == node->list->head) {
                node->list->head = myNode->next;
            } else {
                temp->next = myNode->next;
                node->list->records--;
                free(myNode);
                break;
            }
        }
        temp = myNode;
        myNode = myNode->next;
    }
    if (flag==1) {
        printf("Cdr %s deleted from list\n\n", ptr->cdr_uniq_id);
    } else {
        printf("No cdr with id: %s in the list\n",ptr->cdr_uniq_id);
    }
}

int contains_number(char number[15],struct list *linked_list) {          //This function makes sure a number is only added once in the list(Used for different operations of the application).

    int flag = 0;
    struct listNode *myNode;
    myNode = linked_list->head;

    if (!myNode) {
        printf("No cdr records with this number\n");
        return 0;
    }

    while (myNode != NULL) {
        if (strcmp(myNode->number,number)==0) {
            flag = 1;
            break;
        }
        myNode = myNode->next;
    }

    if (flag==1) {
        return 1;
    } else {
        return 0;
    }
}

void insertToListIndist(char number[15], struct list *linked_list) {        //Optional Insert function used for the Indist queries.
    struct listNode *newnode = createLinkNodeIndist(number);
    if (linked_list->records==0) {
        linked_list->head = newnode;
        linked_list->records++;
        return;
    }
    newnode->next = linked_list->head;
    linked_list->head = newnode;
    linked_list->records++;
    return;
}

struct listNode * createLinkNodeIndist(char number[15]) {       //Optional Link Creating function used for the Indist queries.
    struct listNode *newnode;
    newnode = malloc(sizeof (struct listNode));
    strcpy(newnode->number,number);
    newnode->next = NULL;
    return newnode;
}

void printIndist(struct listNode *myNode) {             //Optional Print function used for the Indist queries.
    while (myNode != NULL) {
        printf("number  : %s\n", myNode->number);
        myNode = myNode->next;
    }
}

void print_listNode(struct hashNode *ptr,int table) {       //Print function that assists the print(hashTable) queries.
    struct listNode *myNode;
    myNode = ptr->list->head;
    if (!myNode) {
        printf("Search element unavailable in hash table\n");
        return;
    }

    while (myNode != NULL) {
            printf("CDR ID             : %s\n", myNode->cdr_uniq_id);
        if(table==1) {
            printf("Number(Destination): %s\n", myNode->number);
        } else {
            printf("Number(Originator) : %s\n", myNode->number);
        }
            printf("Date               : %s\n", myNode->date);
            printf("Time               : %s\n", myNode->init_time);
            printf("Duration           : %d\n", myNode->duration);
            printf("Type               : %d\n", myNode->type);
            printf("Tarrif             : %d\n", myNode->tarrif);
            printf("Fault Condition    : %d\n\n", myNode->fault_condition);
            myNode = myNode->next;
    }
}
