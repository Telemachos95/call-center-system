#ifndef LINKED_LIST_H
#define	LINKED_LIST_H

#include "structs.h"
#include "chained_hashing.h"


struct listNode {
    char *cdr_uniq_id;
    char number[15];
    char date[9];
    char init_time[6];                  //Every node on the list has the rest cdr info and a pointer to the next node.
    int duration;
    int type;
    int tarrif;
    int fault_condition;
    struct listNode *next;
};

struct list {
    struct listNode *head;              //A typical linked list implementation,the first node is always called head,and we keep track of the number of elements in the list.
    int records;                        //Every hashNode has a linked list for storing the remaining cdr information.
};

struct listNode * createLinkNode(struct cdr *ptr,int table);
void insertToList(struct cdr *ptr, struct hashNode *ptr1,int table);
void  delete_listNode(struct cdr *ptr,struct hashNode *node);
int contains_number(char number[15],struct list *linked_list);
struct listNode * createLinkNodeIndist(char number[15]);
void insertToListIndist(char number[15], struct list *linked_list);
void printIndist(struct listNode *myNode);
void print_listNode(struct hashNode *ptr,int table);

#endif
